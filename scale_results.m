function Results = scale_results(Results, roiRows, roiCols, filePrefix)
  % Constant
  DICOM_MAX = 2^12 - 1; % dicom is 12 bit

  % Scale NUFFT down to the desired max
  [Results.nufftScaled, nufftScale] = scale_down(Results.nufftImage, DICOM_MAX);

  % ROI
  nufftROI = Results.nufftScaled(roiRows, roiCols, :);
  grid3ROI = Results.grid3Image(roiRows, roiCols, :);
  grogROI  = Results.grogImage(roiRows, roiCols, :);
  nnROI    = Results.nnImage(roiRows, roiCols, :);

  % Display ROI
  % figure('Name', [sliceNo ' ROI'])
  % imagesc(nufftROI(:,:,1))

  % scale factor - minimize RMSE from the newly scaled NUFFT in the ROI
  denominator = nufftROI(:).' * pinv(nufftROI(:)).'; % should be 1
  grid3Scale = nufftROI(:).' * pinv(grid3ROI(:)).' / denominator;
  grogScale = nufftROI(:).' * pinv(grogROI(:)).' / denominator;
  nnScale = nufftROI(:).' * pinv(nnROI(:)).' / denominator;

  % Save scale factors
  save(['../+Shared/data/' filePrefix '_scale_factors.mat'], 'grid3Scale', 'grogScale', 'nnScale', 'nufftScale')

  % scale'em
  Results.grid3Scaled = Results.grid3Image * grid3Scale;
  Results.grogScaled = Results.grogImage * grogScale;
  Results.nnScaled = Results.nnImage * nnScale;
end

function [dataOutput, scaleFactor] = scale_down(dataInput, desiredMax)
  %  Scale down if max is greater than desiredMax
  theMax = max(dataInput(:));
  if theMax > desiredMax
    scaleFactor = (desiredMax / theMax);
  else
    scaleFactor = 1;
  end
  dataOutput = dataInput * scaleFactor;
end
