function pre_process_data(kSpace, timeFrames, prefix)

  N_COMPONENTS = 8;    % How many coils to compress down to
  K_POWER_OF_TEN = 3;  % Scaled order of magnitude - 3 means 1000

  % Assumes zslices at end of volume
  nSlices = size(kSpace, ndims(kSpace));

  for iSlice = 1:nSlices
    kSpaceSlice = get_slice(kSpace, iSlice);
    trajectory = get_trajectory_gold(kSpaceSlice);
    [kSpaceSlice, trajectory] = select_frames(kSpaceSlice, trajectory, timeFrames);
    kSpaceSlice = scale_data(kSpaceSlice, K_POWER_OF_TEN);
    kSpaceSlice = pca_compress(kSpaceSlice, N_COMPONENTS);
    save_file(iSlice, prefix, kSpaceSlice, trajectory);
  end
end

function kSpaceSlice = get_slice(kSpace, iSlice)
  % Convert to double and put coils on end
  kSpaceSlice = double(kSpace(:,:,:,:,iSlice));
  kSpaceSlice = permute(kSpaceSlice, [1 2 4 3]);
end

function [kSpaceSlice, trajectory] = select_frames(kSpaceSlice, trajectory, timeFrames)
  kSpaceSlice = kSpaceSlice(:,:,timeFrames,:);
  trajectory = trajectory(:,:,timeFrames);
end

function kSpaceSlice = pca_compress(kSpaceSlice, nComponents)
  % compress given coils down to representative virtual coils
  [nReadout, nRay, nTime, nCoil] = size(kSpaceSlice);
  nDataPoints = nReadout*nRay*nTime;
  coilData = reshape(kSpaceSlice, [nDataPoints nCoil]);
  coefficients = princomp(coilData);
  coefficients = coefficients(:, 1:nComponents);
  compressedData = coilData * coefficients;
  outputShape = [nReadout nRay nTime nComponents];
  kSpaceSlice = reshape(compressedData, outputShape);
end

function kSpaceSlice = scale_data(kSpaceSlice, powerOfTen)
  % Scale data so that new max is around 10^powerOfTen
  maximum = max(abs(kSpaceSlice(:)));
  orderOfMagnitude = log(maximum) ./ log(10);
  powerFactor = powerOfTen - floor(orderOfMagnitude);
  kSpaceSlice = kSpaceSlice * 10 ^ powerFactor;
end

function save_file(iSlice, prefix, kSpaceSlice, trajectory)
  filename = [prefix '_slice_' num2str(iSlice) '.mat'];
  save(filename, 'kSpaceSlice', 'trajectory');
end
