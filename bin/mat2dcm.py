#!/Users/UCAIR/anaconda/bin/python
import dicom
import glob
import os
import sys
import datetime
import scipy.io
import numpy as np
import ConfigParser
from optparse import OptionParser

# ============================================================================
class Mat2DcmINIMatFlag:
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
    def __init__(self):
        self.transform = False
        self.transpose_dim12 = False
        self.truncate_and_center = True
        self.sort_by_filename = True

# ============================================================================
class Mat2DcmINIMat:
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
    def __init__(self):
        self.n_file = 0
        self.files = []
        self.flag = Mat2DcmINIMatFlag()
        self.dir = ''
        self.volume_name = ''
        self.scale = 1.0
        self.transforms = []
        self.i1 = 0
        self.i2 = 0
        self.i3 = 0
        self.dim1 = []
        self.dim2 = []
        self.dim3 = []

# ============================================================================
class Mat2DcmINIDcmIn:
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
    def __init__(self):
        self.n_dir = 0
        self.dirs = []
        self.sort_by = 'filename'
        self.dim1 = []
        self.dim2 = []
        self.dim3 = []

# ============================================================================
class Mat2DcmINIDcmOutFlag:
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
    def __init__(self):
        self.update_study_uid = False
        self.update_series_uid = True
        self.update_sop_instance_uid = True
        self.update_media_storage_sop_instance_uid = True
        self.update_series_desc = False

# ============================================================================
class Mat2DcmINIDcmOut:
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
    def __init__(self):
        self.series_desc_head = ''
        self.dir = ''
        self.flag = Mat2DcmINIDcmOutFlag()
        self.dim1 = []
        self.dim2 = []
        self.dim3 = []
        self.series_increment = 0

# ============================================================================
class Mat2DcmINI:
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------

    # -------------------------------------------------
    # Sets up default ini file
    def __init__(self, matlab=None, dcm_in=None, dcm_out=None):

        self.config = ConfigParser.SafeConfigParser(allow_no_value = True)
        self.config.optionxform = str

        self.config.add_section('Matlab')
        self.config.set('Matlab','# Variable substitutions have the form %(varname)s')
        self.config.set('Matlab','dir_stub','./')
        self.config.set('Matlab','# Indicates which matlab .mat files we want from our input directory')
        self.config.set('Matlab','filename_glob','STCR_*.mat')
        self.config.set('Matlab','# Method to use for sorting mat files when matching against dcm_dirs')
        self.config.set('Matlab','sort_by','filename')
        self.config.set('Matlab','# Name of variable in matlab files which stores the image volume')
        self.config.set('Matlab','image_volume_name','some_matlab_variable_name')
        self.config.set('Matlab','# Scale factor to apply to matlab data prior to conversion to 12-bit')
        self.config.set('Matlab','scale_before_conversion','5.0')
        self.config.set('Matlab','# Location will be scanned for filename_glob files')
        self.config.set('Matlab','dir','%(dir_stub)s/some/directory/of/mat/files')
        self.config.set('Matlab','# Transformations: Transform the data prior to output')
        self.config.set('Matlab','#     transform## if present and transform01 is not NONE')
        self.config.set('Matlab','#     transforms are performed in order of ascending ##')
        self.config.set('Matlab','# Possible transforms, dimensions left to right (1,2,3):')
        self.config.set('Matlab','#     reverse_dim1')
        self.config.set('Matlab','#     reverse_dim2')
        self.config.set('Matlab','#     reverse_dim3')
        self.config.set('Matlab','#     transpose_dim12')
        self.config.set('Matlab','transform01','NONE')
        self.config.set('Matlab','#transform01 = reverse_dim1')
        self.config.set('Matlab','transform02','reverse_dim2')
        self.config.set('Matlab','# If incoming matlab is larger than incoming DICOM')
        self.config.set('Matlab','#     we take the center of the matlab along XY')
        self.config.set('Matlab','#     based on the dimensions of the incoming DICOM')
        self.config.set('Matlab','truncate_and_center','true')
        self.config.set('Matlab','# Otherwise, take a window starting at matlab coordinates')
        self.config.set('Matlab','#    dim1_start,dim2_start,dim3_start where the size is')
        self.config.set('Matlab','#    dictated by the incoming DICOM image size')
        self.config.set('Matlab','# These values ignored if truncate_and_center = true')
        self.config.set('Matlab','# These values refer to data after transforms above.')
        self.config.set('Matlab','dim1_start','1')
        self.config.set('Matlab','dim2_start','1')
        self.config.set('Matlab','dim3_start','1')

        self.config.add_section('DICOM Input')
        self.config.set('DICOM Input','# Variable substitutions have the form %(varname)s')
        self.config.set('DICOM Input','dir_stub','./')
        self.config.set('DICOM Input','# Glob string for identifying the dicom')
        self.config.set('DICOM Input','filename_glob','*.dcm')
        self.config.set('DICOM Input','# Method to use for sorting dcm files when assigning to sequential slices in')
        self.config.set('DICOM Input','#     mat files')
        self.config.set('DICOM Input','sort_by','filename')
        self.config.set('DICOM Input','# We need one series of DICOM files per each matlab *.mat file found in')
        self.config.set('DICOM Input','#     [Matlab].dir')
        self.config.set('DICOM Input','dir_01','%(dir_stub)s/some/directory/of/dicoms')

        self.config.add_section('DICOM Output')
        self.config.set('DICOM Output','# Variable substitutions have the form %(varname)s')
        self.config.set('DICOM Output','dir_stub','./')
        self.config.set('DICOM Output','dir','%(dir_stub)s/some/output/directory')
        self.config.set('DICOM Output','# Whether to increment the series number.')
        self.config.set('DICOM Output','#     Generally, the series_uid should address')
        self.config.set('DICOM Output','#     uniqueness here, so no increment should be necessary')
        self.config.set('DICOM Output','series_number_increment','0')
        self.config.set('DICOM Output','# Generate new study UID tends to place in a different exam')
        self.config.set('DICOM Output','#     within Osirix')
        self.config.set('DICOM Output','update_study_uid','false')
        self.config.set('DICOM Output','# Generate new series uid creates a new series in the original exam')
        self.config.set('DICOM Output','update_series_uid','true')
        self.config.set('DICOM Output','# These are useful as true to conform to DICOM standard')
        self.config.set('DICOM Output','update_sop_instance_uid','true')
        self.config.set('DICOM Output','update_media_storage_sop_instance_uid','true')
        self.config.set('DICOM Output','# Whether to change the existing series description')
        self.config.set('DICOM Output','#     When true, original description will be saved in the image comment')
        self.config.set('DICOM Output','series_description_update','false')
        self.config.set('DICOM Output','# If updating, then this stub will be used plus the series number')
        self.config.set('DICOM Output','series_description_stub','Some series description')

        if matlab is not None:
            for key, value in matlab:
                self.config.set('Matlab', key, value)
        if dcm_in is not None:
            for key, value in dcm_in:
                self.config.set('DICOM Input', key, value)
        if dcm_out is not None:
            for key, value in dcm_out:
                self.config.set('DICOM Output', key, value)

    # -------------------------------------------------
    # Read in file, replacing default parameters
    def load(self,filename, matlab=None, dcm_in=None, dcm_out=None):
        self.config.read(filename)
        if matlab is not None:
            for key, value in matlab:
                self.config.set('Matlab', key, value)
        if dcm_in is not None:
            for key, value in dcm_in:
                self.config.set('DICOM Input', key, value)
        if dcm_out is not None:
            for key, value in dcm_out:
                self.config.set('DICOM Output', key, value)

    # -------------------------------------------------
    def write(self,filename):
        with open(filename, 'wb') as configfile:
            my_ini.config.write(configfile)

    # -------------------------------------------------
    # Inserts ini parameters into useful class members
    #     These operations interact only with the ini file
    def ini2control(self):

        self.mat = Mat2DcmINIMat()
        self.dcm_in = Mat2DcmINIDcmIn()
        self.dcm_out = Mat2DcmINIDcmOut()

        self.dcm_out.series_desc_head = \
            self.config.get('DICOM Output','series_description_stub')
        self.mat.dir = self.config.get('Matlab','dir')
        self.dcm_out.dir = self.config.get('DICOM Output','dir')
        self.mat.volume_name = \
            self.config.get('Matlab','image_volume_name')
        self.mat.scale = \
            self.config.getfloat('Matlab', 'scale_before_conversion')
        self.dcm_out.flag.update_study_uid = \
            self.config.getboolean('DICOM Output','update_study_uid')
        self.dcm_out.flag.update_series_uid = \
            self.config.getboolean('DICOM Output','update_series_uid')
        self.dcm_out.flag.update_sop_instance_uid = \
            self.config.getboolean('DICOM Output','update_sop_instance_uid')
        self.dcm_out.flag.update_media_storage_sop_instance_uid = \
            self.config.getboolean('DICOM Output',
                                   'update_media_storage_sop_instance_uid')
        self.dcm_out.flag.update_series_desc = \
            self.config.getboolean('DICOM Output', 'series_description_update')

        # Transformations?
        self.mat.flag.transform = \
            self.config.get('Matlab', 'transform01').upper() != 'NONE'
        self.mat.flag.transpose_dim12 = False
        if self.mat.flag.transform:
            self.mat.transforms = []
            matlab_options = self.config.options('Matlab')
            transform_options = [matlab_option for matlab_option in
                                 self.config.options('Matlab')
                                 if matlab_option.startswith('transform')]
            transform_options.sort()
            for transform_option in transform_options:
                matlab_transform = self.config.get('Matlab',transform_option)
                if matlab_transform != 'None':
                    self.mat.transforms.append(matlab_transform)
                else:
                    break
                if matlab_transform == 'transpose_dim12':
                    self.mat.flag.transpose_dim12 = \
                        not self.mat.flag.transpose_dim12

        # Value to add to incoming series numbers to create new series
        self.dcm_out.series_increment = \
            np.int(self.config.get('DICOM Output', 'series_number_increment'))
        # Source directories for dcm files, assumed to be numbered
        # nDir X nFilePerDir should match number of mat files in self.mat.dir
        self.dcm_in.dirs = []
        dicom_dir_options = [dicom_dir_option for dicom_dir_option in
                             self.config.options('DICOM Input')
                             if (dicom_dir_option.startswith('dir_') and
                                 (dicom_dir_option != 'dir_stub'))]
        dicom_dir_options.sort()
        for dicom_dir_option in dicom_dir_options:
            self.dcm_in.dirs.append(self.config.get('DICOM Input',
                                                    dicom_dir_option))
        self.dcm_in.n_dir = len(self.dcm_in.dirs)

        # Custom ranges for selecting subset of dicoms like we do in self-gated
        # data. If specifying explicit integers then use custom_range_01 etc if giving a
        # mat file name use range_01
        self.dcm_in.custom_ranges = []

        custom_range_options = [custom_range_option for custom_range_option in
                                self.config.options('DICOM Input')
                                if custom_range_option.startswith('custom_range_')]
        custom_range_options.sort()

        for custom_range_option in custom_range_options:
            custom_range_str = self.config.get('DICOM Input', custom_range_option)
            custom_range_arr = [int(s) - 1 for s in custom_range_str.split(',')]

            self.dcm_in.custom_ranges.append(custom_range_arr)

        # If no custom _range given how about a range file name?
        if not self.dcm_in.custom_ranges:
            custom_range_options = [custom_range_option for custom_range_option in
                                    self.config.options('DICOM Input')
                                    if custom_range_option.startswith('range_')]
            custom_range_options.sort()

            for custom_range_option in custom_range_options:
                custom_range_filename = self.config.get('DICOM Input', custom_range_option)

                custom_range_dict = scipy.io.loadmat(custom_range_filename)
                var_name = custom_range_dict.keys()[0] # assume only one variable in mat file
                custom_range_arr = custom_range_dict[var_name][0]
                # Convert from matlab indexing to normal indexing
                custom_range_arr = [s - 1 for s in custom_range_arr]
                self.dcm_in.custom_ranges.append(custom_range_arr)

            # If custom_ranges isn't empty but the length is wrong then error out
            if (self.dcm_in.n_dir != len(self.dcm_in.custom_ranges)) and self.dcm_in.custom_ranges:
                print 'Must have same number of custom ranges as dicom directories'
                sys.exit(1)


        # Output truncation
        self.mat.flag.truncate_and_center = \
            self.config.getboolean('Matlab','truncate_and_center')
        if not self.mat.flag.truncate_and_center:
            self.mat.i1 = np.int(self.config.getint('Matlab','dim1_start') - 1)
            self.mat.i2 = np.int(self.config.getint('Matlab','dim2_start') - 1)
            self.mat.i3 = np.int(self.config.getint('Matlab','dim3_start') - 1)
        else:
            self.mat.i1 = 0
            self.mat.i2 = 0
            self.mat.i3 = 0

        self.mat.flag.sort_by_filename = \
            self.config.get('Matlab', 'sort_by') == 'filename'
        if not self.mat.flag.sort_by_filename:
            print 'Mat file sort not supported: ' + \
                config.get('Matlab', 'sort_by')
            sys.exit(1)

        self.dcm_in.sort_by = self.config.get('DICOM Input', 'sort_by')
        if not self.dcm_in.sort_by == 'filename':
            print 'DICOM file sort not supported: ' + dcm_in_sort
            sys.exit(2)

    # -------------------------------------------------
    # Now address input files, reading them for dimensions
    def get_input_files_and_dim(self):
        # Poll the names of the mat created mat
        self.mat.files = glob.glob(self.mat.dir + '/' + \
                            self.config.get('Matlab','filename_glob'))
        if my_ini.mat.flag.sort_by_filename:
            self.mat.files.sort()
        self.mat.n_file = len(self.mat.files)
        if self.dcm_in.n_dir != self.mat.n_file:
            print 'Mismatch between number of mat files and dcm directories'
            for dcm_dir in self.dcm_in.dirs:
                print dcm_dir
            for mat_file in self.mat.files:
                print mat_file
            sys.exit(3)
        self.mat.dim1 = np.zeros(self.mat.n_file,np.uint32)
        self.mat.dim2 = np.zeros(self.mat.n_file,np.uint32)
        self.mat.dim3 = np.zeros(self.mat.n_file,np.uint32)
        # Determine data dimensions
        for i_file_mat in range(self.mat.n_file):
            mat_file_dict = scipy.io.loadmat(self.mat.files[i_file_mat])
            if self.mat.flag.transpose_dim12:
                self.mat.dim1[i_file_mat] = \
                    mat_file_dict[self.mat.volume_name].shape[1]
                self.mat.dim2[i_file_mat] = \
                    mat_file_dict[self.mat.volume_name].shape[0]
            else:
                self.mat.dim1[i_file_mat] = \
                    mat_file_dict[self.mat.volume_name].shape[0]
                self.mat.dim2[i_file_mat] = \
                    mat_file_dict[self.mat.volume_name].shape[1]
            self.mat.dim3[i_file_mat] = \
                mat_file_dict[self.mat.volume_name].shape[2]

        # Poll and extract dcm dimensions
        self.map_dcm_files_to_mat = []
        self.dcm_in.dim1 = np.zeros(self.dcm_in.n_dir,np.uint32)
        self.dcm_in.dim2 = np.zeros(self.dcm_in.n_dir,np.uint32)
        self.dcm_in.dim3 = np.zeros(self.dcm_in.n_dir,np.uint32)
        for i_dir_dcm in range(self.dcm_in.n_dir):
            dcm_local_files = glob.glob(self.dcm_in.dirs[i_dir_dcm] + '/' + \
                                self.config.get('DICOM Input','filename_glob'))
            # Sort by filename
            if my_ini.dcm_in.sort_by == 'filename':
                dcm_local_files.sort()

            # Get specified indices if custom_ranges isn't empty
            if self.dcm_in.custom_ranges:
                 custom_indices = self.dcm_in.custom_ranges[i_dir_dcm]
                 dcm_local_files = [dcm_local_files[i] for i in custom_indices]

            self.dcm_in.dim3[i_dir_dcm] = len(dcm_local_files)
            dcm_file_info = {}
            dcm_file_info['names'] = dcm_local_files
            self.map_dcm_files_to_mat.append(dcm_file_info)
            n_dcm_files = len(dcm_local_files)
            self.dcm_in.dim3[i_dir_dcm] = n_dcm_files
            if (self.dcm_in.dim3[i_dir_dcm] <
                    (self.mat.dim3[i_dir_dcm] - self.mat.i3)):
                print 'Fewer dicom found than number of slices in matlab file'
                print self.mat.files[i_dir_dcm]
                print n_dcm_files
                sys.exit(4)
            for i_file_dcm in range(n_dcm_files):
                dcm_data = dicom.read_file(dcm_local_files[i_file_dcm])
                if i_file_dcm == 0:
                    self.dcm_in.dim1[i_dir_dcm] = dcm_data.Rows
                    self.dcm_in.dim2[i_dir_dcm] = dcm_data.Columns
                else:
                    if ((self.dcm_in.dim1[i_dir_dcm] != dcm_data.Rows) or
                         (self.dcm_in.dim2[i_dir_dcm] != dcm_data.Columns)):
                        print 'Inconsistent column or row dimensions ' + \
                                  'in dcm directory'
                        print self.dcm_in.dirs[i_dir_dcm]
                        print dcm_local_files[0]
                        print self.dcm_in.dim1[i_dir_dcm], \
                            self.dcm_in.dim2[i_dir_dcm]
                        print dcm_local_files[i_file_dcm]
                        print dcm_data.Rows, dcm_data.Columns
                        sys.exit(5)

    # -------------------------------------------------
    def compute_output_dim(self):
        self.dcm_out.dim1 = np.zeros(self.dcm_in.n_dir,np.uint32)
        self.dcm_out.dim2 = np.zeros(self.dcm_in.n_dir,np.uint32)
        self.dcm_out.dim3 = np.zeros(self.dcm_in.n_dir,np.uint32)
        for i_file_mat in range(self.mat.n_file):
            if self.mat.flag.truncate_and_center:
                if self.mat.dim1[i_file_mat] > self.dcm_in.dim1[i_file_mat]:
                    self.mat.i1 = np.floor((self.mat.dim1[i_file_mat] - \
                                            self.dcm_in.dim1[i_file_mat]) / 2)
                    self.dcm_out.dim1[i_file_mat]=self.dcm_in.dim1[i_file_mat]
                else:
                    self.dcm_out.dim1[i_file_mat]=self.dcm_in.dim1[i_file_mat]
                if self.mat.dim2[i_file_mat] > self.dcm_in.dim2[i_file_mat]:
                    self.mat.i2 = np.floor((self.mat.dim2[i_file_mat] - \
                                            self.dcm_in.dim2[i_file_mat]) / 2)
                    self.dcm_out.dim2[i_file_mat]=self.dcm_in.dim2[i_file_mat]
                else:
                    self.dcm_out.dim2[i_file_mat]=self.dcm_in.dim2[i_file_mat]
            else:
                self.dcm_out.dim1[i_file_mat] = \
                    self.mat.dim1[i_file_mat] - self.mat.i1
                self.dcm_out.dim2[i_file_mat] = \
                    self.mat.dim2[i_file_mat] - self.mat.i2
            self.dcm_out.dim3[i_file_mat] = \
                self.mat.dim3[i_file_mat] - self.mat.i3
        # Update ini
        self.config.set('Matlab', 'dim1_start', str(np.int(self.mat.i1 + 1)))
        self.config.set('Matlab', 'dim2_start', str(np.int(self.mat.i2 + 1)))
        self.config.set('Matlab', 'dim3_start', str(np.int(self.mat.i3 + 1)))

# ---------------------------------------------------------
# Main Program
# ---------------------------------------------------------

# Print out start of program to make it easier to distinguish between our output and the user's input.
print '\n=============\n   mat2dcm\n============='

usage = "usage: %prog [options] (-mio may repeat for different key value pairs)"
command_line = OptionParser(usage=usage)
command_line.add_option("-f", "--file", dest="file_ini",
                        action="store", type="string",
                        help="ini file will be FILE", metavar="FILE")
command_line.add_option("-m", "--matlab", dest="Matlab",
                   nargs=2, action="append", metavar="key value",
                   help="Example: -m dir_stub ./this/dir")
command_line.add_option("-i", "--dicomin", dest="dcm_in",
                   nargs=2, action="append", metavar="key value",
                   help="Example: -i filename_glob *.dcm")
command_line.add_option("-o", "--dicomout", dest="dcm_out",
                   nargs=2, action="append", metavar="key value",
                   help="Example: -o series_number_increment 1")
(command_line_options, args) = command_line.parse_args()
if command_line_options.file_ini is not None:
    file_ini = command_line_options.file_ini
else:
    file_ini = 'mat2dcm.ini'

datetime_str = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
my_ini = Mat2DcmINI(matlab=command_line_options.Matlab,
                    dcm_in=command_line_options.dcm_in,
                    dcm_out=command_line_options.dcm_out)

if not os.path.exists(file_ini):
    my_ini.write(file_ini)
    sys.exit(0)

my_ini.load(file_ini,
            matlab=command_line_options.Matlab,
            dcm_in=command_line_options.dcm_in,
            dcm_out=command_line_options.dcm_out)

my_ini.ini2control()

my_ini.get_input_files_and_dim()

my_ini.compute_output_dim()

# ---------------------------------------------------------
# Loop through input mat and dicom, replacing pixel data
#     and outputting new dicom to mat directory
# ---------------------------------------------------------
if not os.path.isdir(my_ini.dcm_out.dir):
    os.makedirs(my_ini.dcm_out.dir)
mat_root = os.path.basename(my_ini.mat.dir)
if my_ini.dcm_out.flag.update_study_uid:
    study_uid = dicom.UID.generate_uid()
for i_file_mat in range(my_ini.mat.n_file):

    if my_ini.dcm_out.flag.update_series_uid:
        series_uid = dicom.UID.generate_uid()
    mat_file_dict = scipy.io.loadmat(my_ini.mat.files[i_file_mat])
    data = mat_file_dict[my_ini.mat.volume_name]
    if my_ini.mat.flag.transform:
        for matlab_transform in my_ini.mat.transforms:
            if matlab_transform == 'reverse_dim1':
                data = data[::-1,:,:]
            elif matlab_transform == 'reverse_dim2':
                data = data[:,::-1,:]
            elif matlab_transform == 'reverse_dim3':
                data = data[:,:,::-1]
            elif matlab_transform == 'transpose_dim12':
                data = np.transpose(data,(1,0,2))
            else:
                print 'Unknown transform ' + matlab_transform
                sys.exit(6)

    dcm_filenames = my_ini.map_dcm_files_to_mat[i_file_mat]['names']

    for i_dim3 in range(my_ini.mat.dim3[i_file_mat]):

        dcm_file = dcm_filenames[i_dim3]

        # Read the mat
        mat_data = np.int16(
            my_ini.mat.scale *
            data[my_ini.mat.i1:my_ini.mat.i1+my_ini.dcm_out.dim1[i_file_mat],
                 my_ini.mat.i2:my_ini.mat.i2+my_ini.dcm_out.dim2[i_file_mat],
                 my_ini.mat.i3+i_dim3])

        # Read and adjust the dicom data
        dcm_data = dicom.read_file(dcm_file)
        dcm_data.SeriesNumber = \
            dcm_data.SeriesNumber + my_ini.dcm_out.series_increment
        dcm_data.Rows = my_ini.dcm_out.dim1[i_file_mat]
        dcm_data.Columns = my_ini.dcm_out.dim2[i_file_mat]
        dcm_data.PixelData = mat_data.tostring()
        if my_ini.dcm_out.flag.update_series_desc:
            dcm_data.ImageComment = dcm_data.SeriesDescription
            dcm_data.SeriesDescription = my_ini.dcm_out.series_desc_head + \
                                         " ("+str(dcm_data.SeriesNumber)+")"
        dcm_data.AccessionNumber = datetime_str
        dcm_data.StudyID = datetime_str
        dcm_uid = dicom.UID.generate_uid()
        if my_ini.dcm_out.flag.update_sop_instance_uid:
            dcm_data.SOPInstanceUID = dcm_uid
        if my_ini.dcm_out.flag.update_media_storage_sop_instance_uid:
            dcm_data.MediaStorageSOPInstanceUID = dcm_uid
        if my_ini.dcm_out.flag.update_study_uid:
            dcm_data.StudyInstanceUID = study_uid
        if my_ini.dcm_out.flag.update_series_uid:
            dcm_data.SeriesInstanceUID = series_uid

        # Save the new dcm
        dcm_filename = datetime_str + \
            '_s{0:04d}_'.format(dcm_data.SeriesNumber) + \
            'i{0:04d}_'.format(dcm_data.InstanceNumber) + mat_root + '.dcm'
        dcm_out = my_ini.dcm_out.dir + '/' + dcm_filename
        print 'Writing ' + dcm_filename
        dcm_data.save_as(dcm_out)

# Save the ini file in all three locations
my_ini.write(my_ini.mat.dir + '/mat2dcm.ini.' + datetime_str)
#my_ini.write(my_ini.dcm_dir_in + '/mat2dcm.ini.' + datetime_str)
my_ini.write(my_ini.dcm_out.dir + '/mat2dcm.ini.' + datetime_str)
