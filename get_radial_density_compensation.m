function dens_comp = get_radial_density_compensation(k_space)
  % create density compensation for radial data
  Nro = size(k_space,1);
  Nray = size(k_space,2);
  dens_comp = Shared.designFilter('ram-lak', Nro, 1);
  dens_comp = repmat(dens_comp,1,Nray);
  dens_comp = fftshift(dens_comp,1);
end
