function trajectory = get_trajectory_gold( kSpaceIn )
  % Generates golden ratio trajectory k = kx + i ky
  % where k-space is centered at 0 and ranges from -0.5 to 0.5
  % both vertically and horizontally

  GOLDEN_ANGLE = ((sqrt(5)-1)/2)*pi;

  [nReadout, nRay, nTime, nCoil] = size(kSpaceIn);

  % Golden angle thetaArray
  totalRays = nRay * nTime;
  thetaMax = (totalRays - 1) * GOLDEN_ANGLE;
  thetaArray = 0:GOLDEN_ANGLE:thetaMax;
  thetaArray = mod(thetaArray, pi);

  trajectory = zeros(nReadout, nRay, nTime);
  rhoArray = linspace(-0.5, 0.5, nReadout);

  rayIndex = 0;
  for iTime = 1:nTime
    for iRay = 1:nRay
      rayIndex = rayIndex + 1;
      theta = thetaArray(rayIndex);
      projection = cos(theta) + 1i * sin(theta);
      trajectory(:,iRay,iTime) = rhoArray * projection;
    end
  end
end
