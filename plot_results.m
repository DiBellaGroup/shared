function plot_results(Results, sliceNo)
  %% -- Display Results.
  figure('Name', ['slice ' sliceNo ' images'])
  FRAME = 1;
  subplot(2,2,1)
  title('Grid3')
  imagesc(Results.grid3Image(:,:,FRAME)), axis equal, axis tight, colormap gray, brighten(0.5)

  subplot(2,2,2)
  title('scGROG')
  imagesc(Results.grogImage(:,:,FRAME)), axis equal, axis tight

  subplot(2,2,3)
  title('Nearest Neighbor')
  imagesc(Results.nnImage(:,:,FRAME)), axis equal, axis tight

  subplot(2,2,4)
  title('Nearest Neighbor')
  imagesc(Results.nufftImage(:,:,FRAME)), axis equal, axis tight
end
