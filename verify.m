disp('Checking +Shared dependencies')

% State package manager version then import if OK
PackageManagement.verify_self('2')
import PackageManagement.*

% Add package dependencies
verify_package('PreInterpolator', '2');
verify_package('ScGrog', '2');
verify_package('Stcr', '2');
