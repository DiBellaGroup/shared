function dateString = todays_date
  dateString = datestr(now, 'yyyy-mm-dd');
end
