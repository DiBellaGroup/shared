function Results = reconstruct_data(kSpaceSlice, trajectory, sliceNo)
  % kSpaceSlice = kSpaceSlice(:,:,1:3,1:2);
  % trajectory = trajectory(:,:,1:3);

  KSpaceData.kSpace = kSpaceSlice;
  KSpaceData.trajectory = trajectory;

  Weights.cartesian.temporal = 0.03;
  Weights.cartesian.spatial = 0.004;
  Weights.cartesian.fidelity = 1;

  Weights.nufft.temporal = 0.06;
  Weights.nufft.spatial = 0.01;
  Weights.nufft.fidelity = 1;

  [nReadout, ~, nTime, nCoil] = size(kSpaceSlice);
  KSpaceData.cartesianSize = [nReadout, nReadout, nTime, nCoil];
  KSpaceData.sliceNo = sliceNo;

  Results = reconstruct(KSpaceData, Weights);
end

function Results = reconstruct(KSpaceData, Weights)
  Results.grid3Image = reconstruct_with_grid3(KSpaceData, Weights.cartesian);
  Results.grogImage = reconstruct_with_grog(KSpaceData, Weights.cartesian);
  Results.nnImage = reconstruct_with_nn(KSpaceData, Weights.cartesian);
  Results.nufftImage = reconstruct_with_nufft(KSpaceData, Weights.nufft);
end

function grid3Image = reconstruct_with_grid3(KSpaceData, Weights)
  disp('Using Griddata')
  tic
  KSpaceData = PreInterpolator.use_griddata(KSpaceData);
  grid3Image = Stcr.fft_recon_4D(KSpaceData, Weights);
  toc
end

function grogImage = reconstruct_with_grog(KSpaceData, Weights)
  disp('Using GROG')
  [gX, gY] = ScGrog.get_gx_gy(KSpaceData);

  % Now pre-interpolate.
  tic
  KSpaceData = PreInterpolator.use_grog(KSpaceData, gX, gY);
  grogImage = Stcr.fft_recon_4D(KSpaceData, Weights);
  toc
end

function nnImage = reconstruct_with_nn(KSpaceData, Weights)
  disp('Using NN')
  tic
  KSpaceData = PreInterpolator.use_nearest_neighbor(KSpaceData);
  nnImage = Stcr.fft_recon_4D(KSpaceData, Weights);
  toc
end

function nufftImage = reconstruct_with_nufft(KSpaceData, Weights)
  densityCompensation = Shared.get_radial_density_compensation(KSpaceData.kSpace);

  % Now reconstruct.
  disp 'Regridding with nuFFT'
  tic
  nufftImage = Stcr.nufft_recon_4D(KSpaceData, densityCompensation, Weights);
  toc
end
