# Shared Code

This is all of the shared code between the notebooks. It's got to be independently version controlled because over time as the project changes so will the code that's needed. But we'll still be sharing code!

The answer is to explicitly specify dependency on this package and track its version.

### Note on packages

This whole notebook approach to organization isn't well documented because I'm experimenting with it. So I'll at least explain that the rough idea is:

+Shared/
notebook1/
notebook2/
notebook3/
etc.

This way, from any notebook you just need to add the path '../' and then you can call
`Shared.function_name`.

So the notebooks this Shared folder goes with all call it like that and in order to use them you'll need to have this folder on the path and it'll have to have the name `+Shared` to work.

### Dependent Packages

In `verify.m` you can see the packages this package depends on. Including one that handles Package Dependency Management!

These packages are on bitbucket at:
https://bitbucket.org/account/user/DiBellaGroup/projects/MAT
